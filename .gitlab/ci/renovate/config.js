const prBodyColumns = [
  'Package',
  'Type',
  'Update',
  'Change',
];

/**
 * @see https://docs.renovatebot.com/self-hosted-configuration/
 * @see https://docs.renovatebot.com/configuration-options/
 * @see https://docs.renovatebot.com/config-presets/
 */
module.exports = {
  extends: [
    ':pinAllExceptPeerDependencies',
    ':dependencyDashboard',
    ':ignoreModulesAndTests',
    ':semanticCommits',
    ':separateMultipleMajorReleases',
    'group:monorepos',
    'group:recommended',
    'workarounds:all',
  ],
  timezone: 'Europe/Paris',
  autodiscover: true,
  autodiscoverFilter: [
    'ss-open/*',
    'ss-open/!(poc)/**/*',
  ],
  npmrc: '@ss-open:registry=https://gitlab.com/api/v4/packages/npm/',
  persistRepoData: true,
  onboarding: false,
  platform: 'gitlab',
  platformAutomerge: true,
  automergeType: 'pr',
  prConcurrentLimit: 5,
  gitLabIgnoreApprovals: true,
  requireConfig: 'optional',
  commitBodyTable: true,
  stabilityDays: 3,
  major: {
    dependencyDashboardApproval: true,
    gitLabIgnoreApprovals: false,
  },
  prBodyDefinitions: {
    // @see https://github.com/whitesource/merge-confidence/blob/main/beta.json
    Age: "[![age](https://badges.renovateapi.com/packages/{{datasource}}/{{replace '/' '%2f' depName}}/{{{toVersion}}}/age-slim)](https://docs.renovatebot.com/merge-confidence/)",
    Adoption: "[![adoption](https://badges.renovateapi.com/packages/{{datasource}}/{{replace '/' '%2f' depName}}/{{{toVersion}}}/adoption-slim)](https://docs.renovatebot.com/merge-confidence/)",
    Passing: "[![passing](https://badges.renovateapi.com/packages/{{datasource}}/{{replace '/' '%2f' depName}}/{{{toVersion}}}/compatibility-slim/{{{fromVersion}}})](https://docs.renovatebot.com/merge-confidence/)",
    Confidence: "[![confidence](https://badges.renovateapi.com/packages/{{datasource}}/{{replace '/' '%2f' depName}}/{{{toVersion}}}/confidence-slim/{{{fromVersion}}})](https://docs.renovatebot.com/merge-confidence/)",
  },
  // https://sourcegraph.com/search/badge?q=repo:github.com/babel/babel+case:yes+-file:package-lock.json+babel/babel&label=matches
  // https://sourcegraph.com/search?q=repo:github.com/babel/babel+case:yes+-file:package-lock.json+babel/babel
  prBodyColumns,
  hostRules: [
    {
      hostType: 'npm',
      matchHost: 'https://gitlab.com/api/v4/packages/npm/',
      token: process.env.GITLAB_TOKEN,
    },
    {
      matchHost: process.env.CI_REGISTRY,
      username: process.env.GITLAB_USER,
      password: process.env.GITLAB_TOKEN,
    },
  ],
  packageRules: [
    // @see https://github.com/renovatebot/renovate/issues/7879
    {
      matchDatasources: ['maven', 'npm', 'pypi'],
      updateTypes: ['patch', 'minor', 'major'],
      prBodyColumns: [
        ...prBodyColumns,
        'Age',
        'Adoption',
        'Passing',
        'Confidence',
      ],
    },
    {
      matchManagers: [
        'composer',
        'npm',
      ],
      matchDepTypes: [
        'dependencies',
        'peerDependencies',
        'require',
      ],
      semanticCommitType: 'fix',
    },
    // Weekly schedule
    {
      schedule: [
        'before 4am on Monday',
      ],
      matchPackageNames: [
        'bridgecrew/checkov',
        'renovate/renovate',
      ],
    },
    // Monthly schedule
    {
      schedule: [
        'before 4am on the first day of the month',
      ],
      matchPackageNames: [
        'eslint',
      ],
      matchPackagePrefixes: [
        'ss-open/',
        '@ss-open/',
        'registry.gitlab.com/ss-open/',
      ],
    },
    {
      matchPackagePrefixes: [
        'ss-open/',
        '@ss-open/',
      ],
      dependencyDashboardApproval: false,
      stabilityDays: 0,
      prBodyColumns,
    },
    {
      matchManagers: [
        'gitlabci-include',
      ],
      semanticCommitType: 'ci',
      semanticCommitScope: null,
    },
    {
      matchUpdateTypes: [
        'minor',
        'patch',
        'pin',
      ],
      matchDepTypes: [
        'devDependencies',
        'require-dev',
        'repository',
      ],
      matchPackageNames: [
        'eslint',
        'typescript',
      ],
      matchPackagePrefixes: [
        '@ss-open/',
        '@types/',
        '@typescript-eslint/',
        'eslint-',
        'eslint-',
        'ss-open/',
      ],
      matchPackagePatterns: [
        '/ss-open/',
      ],
      automerge: true,
    },
    {
      matchUpdateTypes: [
        'minor',
        'patch',
        'pin',
      ],
      matchPackageNames: [
        'renovate/renovate',
      ],
      automerge: true,
    },
    // The following packages can not be updated because of the Expo constraints.
    // @see https://github.com/renovatebot/renovate/issues/7417
    {
      matchSourceUrlPrefixes: [
        'https://github.com/expo',
      ],
      updateTypes: ['patch', 'minor', 'major'],
      enabled: false,
    },
    {
      matchPackageNames: [
        '@react-native-community/datetimepicker',
        '@types/react-native',
        'react-native',
        'react-native-gesture-handler',
        'react-native-maps',
        'react-native-reanimated',
        'react-native-safe-area-context',
        'react-native-screens',
        'react-native-svg',
        'react-native-web',
      ],
      updateTypes: ['patch', 'minor', 'major'],
      matchManagers: ['npm'],
      enabled: false,
    },
  ],
};
